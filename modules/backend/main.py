"""A flask backend for learning gitlab ci"""
from flask import Flask

app = Flask(__name__)

@app.route('/')
def hello_world():
    """Route for hello world"""
    return '歡迎來到 Flask 專案！'

if __name__ == '__main__':
    app.run()
