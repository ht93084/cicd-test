#!/bin/bash

echo $(pylint --version)

# if [ ! -d "tmp/badges" ] && [ ! -d "tmp/lint" ]; then
#     mkdir -p tmp/badges tmp/lint
#     echo "badges and lint folder are created"
# fi

# default path is /
cd app/backend

echo "Pylinting..."
pylint --exit-zero --output-format=text:tmp/lint/pylint.web.txt --recursive=y .
sed -n 's/^Your code has been rated at \([0-9.]*\)\/.*/\1/p' tmp/lint/pylint.web.txt > tmp/badges/pylint.web.score

echo "Creating badges..."
anybadge --overwrite --label Pylint-Backend --value=$(cat tmp/badges/pylint.web.score) --file=tmp/badges/pylint.web.svg pylint